-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-06-2015 a las 09:23:49
-- Versión del servidor: 5.5.43-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `emmatic`
--
CREATE DATABASE IF NOT EXISTS `emmatic` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `emmatic`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asunto`
--

DROP TABLE IF EXISTS `asunto`;
CREATE TABLE IF NOT EXISTS `asunto` (
  `id_asunto` int(11) NOT NULL AUTO_INCREMENT,
  `asunto` text NOT NULL,
  PRIMARY KEY (`id_asunto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `asunto`
--

INSERT INTO `asunto` (`id_asunto`, `asunto`) VALUES
(1, 'Informes web [url_cliente] - [mes] [anyo]'),
(2, 'Informe anual [url_cliente] - [anyo] ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(50) NOT NULL,
  `web` varchar(50) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `web`) VALUES
(1, 'Sistemas Hidráulicos Fendora', 'www.fendora.es'),
(2, 'SJObuntu S.A.', 'www.sjobuntu.com'),
(3, 'FakeBook S.L.', 'www.fakebook.net');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo`
--

DROP TABLE IF EXISTS `correo`;
CREATE TABLE IF NOT EXISTS `correo` (
  `id_correo` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`id_correo`),
  KEY `id_cliente` (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `correo`
--

INSERT INTO `correo` (`id_correo`, `email`, `id_cliente`) VALUES
(1, 'info@fendora.es', 1),
(2, 'soporte@fendora.es', 1),
(3, 'info@sjobuntu.com', 2),
(4, 'contacto@sjobuntu.com', 2),
(5, 'sorteo@fakebook.net', 3),
(6, 'reclamaciones@fakebook.net', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envio`
--

DROP TABLE IF EXISTS `envio`;
CREATE TABLE IF NOT EXISTS `envio` (
  `id_envio` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `id_asunto` int(11) NOT NULL,
  `id_mensaje` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `fecha_envio` datetime NOT NULL,
  PRIMARY KEY (`id_envio`),
  UNIQUE KEY `id_user` (`id_user`),
  KEY `id_asunto` (`id_asunto`),
  KEY `id_mensaje` (`id_mensaje`),
  KEY `id_cliente` (`id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`id_login`, `user`, `password`) VALUES
(2, 'user', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

DROP TABLE IF EXISTS `mensaje`;
CREATE TABLE IF NOT EXISTS `mensaje` (
  `id_mensaje` int(11) NOT NULL AUTO_INCREMENT,
  `mensaje` text NOT NULL,
  PRIMARY KEY (`id_mensaje`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `mensaje`
--

INSERT INTO `mensaje` (`id_mensaje`, `mensaje`) VALUES
(1, 'Buenos días [nombre_cliente],\r\n\r\nAquí os adjuntamos el informe correspondiente al año [anyo].\r\n\r\nUn saludo,'),
(2, 'Buenos días [nombre_cliente],\r\n\r\nAquí os adjuntamos los informes correspondientes al ultimo mes.\r\n\r\nUn saludo,\r\n');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `correo`
--
ALTER TABLE `correo`
  ADD CONSTRAINT `correo_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `envio`
--
ALTER TABLE `envio`
  ADD CONSTRAINT `envio_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `envio_ibfk_2` FOREIGN KEY (`id_asunto`) REFERENCES `asunto` (`id_asunto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `envio_ibfk_3` FOREIGN KEY (`id_mensaje`) REFERENCES `mensaje` (`id_mensaje`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `envio_ibfk_4` FOREIGN KEY (`id_user`) REFERENCES `login` (`id_login`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
