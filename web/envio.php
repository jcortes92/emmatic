<?php 
require 'includes/header.php';

$id_cliente =  $_GET["id"];

// Database connection
require 'includes/mysqli_connect.php';

// Get clients info
$query = "select id_cliente, nombre_cliente, web from cliente where id_cliente ='".$id_cliente."'";
$result = @mysqli_query($dbc,$query);

// If any, print the results as a table.
if(mysqli_num_rows($result)){

// Fetch and print all the records:
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $_SESSION["id_cliente"] = $id_cliente;
        $_SESSION["nombre_cliente"] = $row["nombre_cliente"];
        $_SESSION["url_cliente"] = $row["web"];
    }
}

// Get clients email info
$query = "select email from correo where id_cliente ='".$id_cliente."'";
$result = @mysqli_query($dbc,$query);

$correos = array();
// If any, print the results as a table.
if(mysqli_num_rows($result)){


// Fetch and print all the records:
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        array_push($correos, $row["email"]);
    }
}

// Get subjects email info
$query = "select asunto from asunto";
$result = @mysqli_query($dbc,$query);

$asuntos = array();
// If any, print the results as a table.
if(mysqli_num_rows($result)){


// Fetch and print all the records:
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        array_push($asuntos, $row["asunto"]);
    }
}

// Get message
$query = "select id_mensaje, mensaje from mensaje";
$result = @mysqli_query($dbc,$query);

$mensajes = array();
// If any, print the results as a table.
if(mysqli_num_rows($result)){


// Fetch and print all the records:
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        array_push($mensajes, $row);
    }
}

// Free up the resources
mysqli_free_result ($result);
?>
<!-- Content -->
<fieldset class="fieldset-envio">
    <legend>Envio a cliente <span class="nombre-cliente"><?php echo $nombre_cliente; ?></span></legend>
    <form method="POST" action="includes/enviarCorreo.php" enctype="multipart/form-data">
        <p class="select-destinatarios">
            <label>Destinatarios: </label>
            <select name='correo' multiple>
                <?php 
                foreach ($correos as $value) {
                    echo "<option value='".$value."'>".$value."</li>";
                }
                ?>
            </select>
        </p>
        <p class="select-asunto">
            <label>Asunto: </label>
            <select name='asunto'>
                <?php 
                foreach ($asuntos as $value) {
                    echo "<option value='".$value."'>".$value."</li>";
                }
                ?>
            </select>
        </p>
        <p class="select-mensaje">
            <label>Mensaje: </label>
            <select name='mensaje'>
                <?php 
                foreach ($mensajes as $value) {
                    echo "<option value='".$value["id_mensaje"]."'>".$value["mensaje"]."</li>";
                }
                ?>
            </select>
        </p>
        <p class="select-archivos">
         <input type="file" name="archivosAdjuntos" multiple>
     </p>
     <input type="submit" class="button" value="Hacer envío">
 </form>
</fieldset>

<?php 
require 'includes/footer.php';
?>