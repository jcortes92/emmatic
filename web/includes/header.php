<?php

// Session stuff
// -----------------------------------
session_start();
if (!isset($_SESSION['username'])) {
    header('location: index.php');
    exit();
}
// -----------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>eMmatic</title>
    <!-- jQuery and jQuery UI -->
    <script src="js/jquery-ui/external/jquery/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.js"></script>
    <link rel="stylesheet" href="js/jquery-ui/jquery-ui.css">

    <!-- Stupid table -->
    <script type="text/javascript" src="js/stupidtable.js"></script>

    <!-- CSS-->
    <link rel="stylesheet" href="css/style.css">

    <!-- Javascript general -->
    <script type="text/javascript" src="js/emmatic-general.js"></script>
</head>
<body>
    <div class="container">
        <header class="header">
            <h1><span class="i">e</span>MMATIC</h1>
            <div class="div-logout button">
                <a href="includes/session_destroy.php">Salir</a>
            </div>
            <?php 
                // Load menu
            include 'includes/menu_top.php'; 
            ?>
        </header>