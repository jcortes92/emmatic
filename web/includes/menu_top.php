<div class="menu-top">
    <?php
    $fileName = basename($_SERVER['PHP_SELF']);
    $type = strtolower($_SESSION['name_type']); 
    ?>
    <!-- If page name corresponds with the nav menú element name, change this element class to 'highlight' -->
    <a href="./main.php" <?php if($fileName=='main.php') echo "class='menu-hightlight'"; ?>>
        Inicio
    </a> |
    <a href="./edita-clientes.php" <?php if($fileName=='edita-clientes.php') echo "class='menu-hightlight'"; ?>>
        Clientes
    </a> |
    <a href="./edita-correos.php" <?php if($fileName=='edita-correos.php') echo "class='menu-hightlight'"; ?>>
        Correos
    </a> |
    <a href="./edita-asuntos.php" <?php if($fileName=='edita-asuntos.php') echo "class='menu-hightlight'"; ?>>
        Asuntos
    </a> |
    <a href="./edita-mensajes.php" <?php if($fileName=='edita-mensajes.php') echo "class='menu-hightlight'"; ?>>
        Mensajes
    </a>
</div>