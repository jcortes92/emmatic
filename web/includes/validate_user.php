<?php

// Mysql connection
require 'includes/mysqli_connect.php';

##Getting the information from index.php
$errors = array();

// Check username
if ( !isset($_POST["username"]) || empty($_POST["username"]) ) {
	$errors[]="Must fill username field. ";
} else {
	$username = mysqli_real_escape_string($dbc,htmlentities(strtolower($_POST["username"])));
}

// Check password
if ( !isset($_POST["password"]) || empty($_POST["password"]) ) {
	$errors[]="Must fill password field. ";
} else {
	$password = htmlentities($_POST["password"]);
}

// If there is not any error, continue
if(empty($errors)){
	$query = "select id_login, password from login where user ='".$username."'";
	$result = @mysqli_query($dbc,$query);

	##we validate if the name that we had introduced exist in database
	if($row = mysqli_fetch_array($result, MYSQL_ASSOC))
	{
		##If is correct we will check if the password is correct too.
		if($row["password"] == SHA1($password))
		{

			##Create session
			session_start();
			
			##We will store the username in a session var.
			$_SESSION['username'] = $username;
			$_SESSION['name_type'] = $row['name_type'];
			$_SESSION['id_user'] = $row['id_login'];
			
			##We will redirect to welcome page main.php
			header("Location: main.php");
		}
		else {
			$errors[]="Password or username is wrong. ";
		}
	}
	else {
		$errors[]="Password or username is wrong. ";
	}

	// Mysql disconnect
	require 'includes/mysqli_disconnect.php';
}
?>