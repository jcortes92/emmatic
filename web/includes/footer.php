<footer>
    Jorge Cortés © <?php echo date("Y"); ?>
</footer>

<!-- Dropzone.js -->
<script type="text/javascript" src="js/dropzone.js"></script>

<!-- Upload files js -->
<script type="text/javascript" src="js/uploadFiles.js"></script>
</body>
</html>