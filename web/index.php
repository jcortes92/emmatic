<?php 

// Inicio de sesión
session_start();

// If there is a session started, destroy it
if(isset($_SESSION['username'])) require 'includes/session_destroy.php';

// Validate login form
// ========================================================================
if($_SERVER['REQUEST_METHOD'] == 'POST')
    require 'includes/validate_user.php';
// ========================================================================

?>

<!DOCTYPE html>
<html lang="es_ES">
<head>
    <meta charset="UTF-8">
    <title> eMMATIC - Login</title>
    <link rel="stylesheet" href="css/style.css">
    <!-- If any, show login errors -->
    <?php if (!empty($errors)) 
    { 
        ?>
        <script>
            alert("<?php
                  foreach($errors as $value){
                    echo $value.'\n';
                }
                echo 'Please check and try again.';
                ?>");
            </script>
            <?php 
        }
        ?>
    </head>
    <body>
        <fieldset class="login">
            <form action="" method="POST">
                <h1><span class="i">e</span>MMATIC</h1>
                <div>
                    <label>Nombre de usuario:</label><br>
                    <input name="username" type="text" autofocus></td>
                </div>
                <div>
                    <label>Contraseña:</label><br>
                    <input name="password" type="password"</td>
                </div>
                <div>
                    <input name="iniciar" class="submit" type="submit" value="Log In">
                </div>
            </form>
        </fieldset>
    </body>
    </html>