$(document).ready(function(){
  // Create jQuery button
  $(".button").button();

  // Make the table sortable
  $("#tableClients").stupidtable();

    // Hightlight row on hover mouse in a table
    $("tr").not(':first').hover(
                                function () {
                                  $(this).css("background","#ffff66");
                                }, 
                                function () {
                                  $(this).css("background","");
                                });

    // Add title tip to sortable table headers
    $(".sortable").attr("title", "Click para ordenar");

    //On submit form
    // $("form").submit(function (e) {
    //   e.preventDefault();
    //   //Send data via AJAX
    //   var datosCorreo = {};
    //   $.ajax({
    //     data: datosCorreo,
    //     type: "text",
    //     type: "POST",
    //     url: "includes/enviarCorreo.php"
    //   })
    //   .done(function(result) {
    //     alert(result);
    //   });
    // })
  });