<?php 
require 'includes/header.php';
?>

<div id="content">
<p class="tip">Página de selección de cliente para realizar envío.</p>
    <div id="centered">
        <div id="content-middle">
            <div id="clients_info">
                <?php
                
                // Database connection
                require 'includes/mysqli_connect.php';
                
                // Get clients info
                $query = "select id_cliente, nombre_cliente, web from cliente";
                $result = @mysqli_query($dbc,$query);
                
                // If any, print the results as a table.
                if(mysqli_num_rows($result)){
                    ?>
                    <div id="table_clients">
                        <table id="tableClients">
                            <thead>
                                <tr class="centered">
                                    <th class="sortable" data-sort="int">ID</th>
                                    <th class="sortable" data-sort="string">Nombre</th>
                                    <th class="sortable" data-sort="string">Web</th>
                                    <th>Acciones disponibles</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 

                                // Fetch and print all the records:
                                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

                                // As the project has no photos database, use the same image for every register
                                    echo '<tr>';

                                    // Show all the values that we want in the table.
                                    foreach ($row as $key => $value) {
                                        echo "<td align='center'>".$value."</td>";
                                    }
                                    echo '<td><a href="envio.php?id='.$row['id_cliente'].'" class="hacerEnvio" id="cliente_'.$row['id_cliente'].'">Hacer envío</a></td></tr>';
                                }
                            }

                            // Free up the resources
                            mysqli_free_result ($result);
                            ?>

                            <!-- Close the table. -->
                        </tbody>
                    </table>
                </div>
            </div>   

        </div>
    </div>
</div>
<?php 
require 'includes/footer.php';
?>