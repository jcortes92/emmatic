#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive

touch track_bootstrap_file.txt
echo -e  "\n\n\t\t------   Initial installations  -------\n\n"


echo  -e  "\n\n\t\tStep 1 of 5 ===> Updating existing packages <===\n\n"
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password 1234'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password 1234'
apt-get update 
echo "step 1 of 5 - packages updated" > track_bootstrap_file.txt


echo  -e  "\n\n\t\tStep 2 of 5 ===> Installing mysql server <===\n\n"
sudo apt-get -y install mysql-server-5.5 php5-mysql 
echo "step 2 of 5 - mysql server installed" >> track_bootstrap_file.txt


echo  -e  "\n\n\t\tStep 3 of 5 ===> Installing Apache server <===\n\n"
apt-get install -y apache2
echo "step 3 of 5 - apache server installed" >> track_bootstrap_file.txt


echo  -e  "\n\n\t\tStep 4 of 5 ===> Installing php5 <===\n\n"
apt-get install  -y  php5 libapache2-mod-php5
service apache2 restart 
echo "step 4 of 5 - php5 installed" >> track_bootstrap_file.txt


echo  -e  "\n\n\t\tStep 5 of 5 ===> Restarting apache2 <===\n\n"
service apache2 restart 
echo "step 5 of 5 - Restarted apache2 server successfully" >> track_bootstrap_file.txt


echo -e "\n--- Installing phpmyadmin ---\n"
apt-get install -y phpmyadmin

echo -e "\n--- Allowing Apache override to all ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

echo "Include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf
 
echo -e "\n--- We definitly need to see the PHP errors, turning them on ---\n"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini
 
 
echo -e "\n--- Configure Apache to use phpmyadmin ---\n"
echo -e "\n\nListen 81\n" >> /etc/apache2/ports.conf
cat > /etc/apache2/conf-available/phpmyadmin.conf << "EOF"
<VirtualHost *:81>
    ServerAdmin webmaster@localhost
    DocumentRoot /usr/share/phpmyadmin
    DirectoryIndex index.php
</VirtualHost>
EOF
a2enconf phpmyadmin > /dev/null 2>&1

echo  -e  "\n\n\n\t\t-----  Wohoo... Your machine configuration is ready now.   -------\n\n"
echo "All operations from bootstrap.sh executed successfully" >> track_bootstrap_file.txt